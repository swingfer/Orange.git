package com.swing.sky.web.generator.domain;

/**
 * 封装一个自动生成的代码模块
 *
 * @author swing
 */
public class Module {

    /**
     * 模块名
     */
    private String moduleName;

    /**
     * 文件后缀名(亦是Java的类名的后缀）
     */
    private String suffix;

    /**
     * 文件扩展名
     */
    private String extension;

    /**
     * 模板文件路径
     */
    private String templatePath;

    /**
     * 文件的绝对路径（文件夹的路径，不包括文件本身名）
     */

    private String absolutePath;

    /**
     * 文件的包名（如果是resources下的文件，则为null)（不包括类名）
     */
    private String packageName;

    /**
     * 文件名(=命名+后缀）（不带扩展名，Java的类名也为此）
     */
    private String fileName;

    @Override
    public String toString() {
        return "Module{" +
                "moduleName='" + moduleName + '\'' +
                ", suffix='" + suffix + '\'' +
                ", templateUrl='" + templatePath + '\'' +
                ", absolutePath='" + absolutePath + '\'' +
                ", packageName='" + packageName + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }


    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templateUrl) {
        this.templatePath = templateUrl;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

}
