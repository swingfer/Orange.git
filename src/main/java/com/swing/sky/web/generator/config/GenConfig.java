package com.swing.sky.web.generator.config;

import com.swing.sky.web.generator.domain.Module;
import com.swing.sky.web.generator.util.Configs;
import com.swing.sky.web.generator.util.OsUtil;

/**
 * @author swing
 */
public class GenConfig {
    public static final String SRC = "src";
    public static final String RESOURCES = "res";

    /**
     * 通过模块名获取一个新的模块实例
     */
    public static Module getModule(String moduleName) {
        Module module = new Module();
        module.setModuleName(moduleName);
        module.setTemplatePath(Configs.readGen(moduleName + "." + "templatePath"));
        String[] split = module.getTemplatePath().split("/");
        String type = split[0];
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < split.length - 1; i++) {
            if (sb.length() == 0)
                sb.append(split[i]);
            else
                sb.append("." + split[i]);
        }
        String subTypeName = sb.toString();
        module.setExtension(type.equals(SRC) ? ".java" : ".xml");
        module.setSuffix(Configs.readSky(moduleName + "." + "suffix"));

        // 如果是源码文件
        String sp = OsUtil.getSeparator();
        if (type.equals(SRC)) {
            module.setPackageName(getBasicPackage() + "." + subTypeName);
            // 绝对路径=输出路径名+打包工具内部源码路径+包名
            module.setAbsolutePath(
                    getOutputPath() + sp + getJavaPath() + sp + module.getPackageName().replace(".", sp));
        }
        // 如果是资源文件
        if (type.equals(RESOURCES)) {
            module.setAbsolutePath(getOutputPath() + sp + getResourcesPath() + sp
                    + subTypeName);
        }
        return module;
    }

    public static String getAuthor() {
        return Configs.readSky("author");
    }

    public static String getBasicPackage() {
        return Configs.readSky("basicPackage");
    }

    public static String getJavaPath() {
        String str = Configs.readSky("src.path");
        return str.replace(".", OsUtil.getSeparator());
    }

    public static String getResourcesPath() {
        String str = Configs.readSky("res.path");
        return str.replace(".", OsUtil.getSeparator());
    }

    public static String getOutputPath() {
        return Configs.readSky("outputPath");
    }

}
