package com.swing.sky.web.generator.util;

public class OsUtil {
    public static String getSeparator() {
        String osName = System.getProperty("os.name");
        if (osName != null) {
            if (osName.startsWith("Linux"))
                return "/";
            else
                return "\\";
        }
        return "\\";
    }
}
