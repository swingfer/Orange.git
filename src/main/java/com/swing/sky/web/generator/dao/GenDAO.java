package com.swing.sky.web.generator.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.swing.sky.web.generator.domain.Column;
import com.swing.sky.web.generator.domain.Schema;

/**
 * 从数据库中获取一些表的基本信息
 *
 * @author swing
 */
public interface GenDAO {
    /**
     * 查询数据库列表
     *
     * @return 列表
     */
    List<Schema> listSchemas();

    /**
     * 查询表备注
     *
     * @param table 信息
     * @return 列表
     */
    String getTableComment(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName);

    /**
     * 查询数据表的列信息列表
     *
     * @param column 业务信息
     * @return 列信息列表
     */
    List<Column> listColumns(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName);
}
