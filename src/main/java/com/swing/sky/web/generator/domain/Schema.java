package com.swing.sky.web.generator.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * 数据库信息
 *
 * @author swing
 */
public class Schema implements Serializable {
    /**
     * 数据库名
     */
    private String schemaName;

    /**
     * 默认编码名
     */
    private String defaultCharacterSetName;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Schema)) {
            return false;
        }
        Schema schema = (Schema) o;
        return Objects.equals(schemaName, schema.schemaName) &&
                Objects.equals(defaultCharacterSetName, schema.defaultCharacterSetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schemaName, defaultCharacterSetName);
    }

    @Override
    public String toString() {
        return "Schema{" +
                "schemaName='" + schemaName + '\'' +
                ", defaultCharacterSetName='" + defaultCharacterSetName + '\'' +
                '}';
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getDefaultCharacterSetName() {
        return defaultCharacterSetName;
    }

    public void setDefaultCharacterSetName(String defaultCharacterSetName) {
        this.defaultCharacterSetName = defaultCharacterSetName;
    }
}
