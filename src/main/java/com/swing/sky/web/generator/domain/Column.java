package com.swing.sky.web.generator.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * 代码生成业务字段
 *
 * @author swing
 */
public class Column implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 列名
     */
    private String columnName;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列的数据类型
     */
    private String dataType;

    /**
     * 列类型(数据类型加上大小）
     */
    private String columnType;

    /**
     * 列标识（PRI:主键 UNI:唯一索引）
     */
    private String columnKey;

    /**
     * 是否可为null(YES：是 NO：否）
     */
    private String nullable;

    /**
     * 默认值(无默认值则为null)
     */
    private String columnDefault;

    /**
     * 顺序位置
     */
    private Long ordinalPosition;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Column)) {
            return false;
        }
        Column that = (Column) o;
        return Objects.equals(columnName, that.columnName) &&
                Objects.equals(columnComment, that.columnComment) &&
                Objects.equals(dataType, that.dataType) &&
                Objects.equals(columnType, that.columnType) &&
                Objects.equals(columnKey, that.columnKey) &&
                Objects.equals(nullable, that.nullable) &&
                Objects.equals(columnDefault, that.columnDefault) &&
                Objects.equals(ordinalPosition, that.ordinalPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnName, columnComment, dataType, columnType, columnKey, nullable, columnDefault,
                ordinalPosition);
    }

    @Override
    public String toString() {
        return "Column{" +
                ", columnName='" + columnName + '\'' +
                ", columnComment='" + columnComment + '\'' +
                ", dataType='" + dataType + '\'' +
                ", columnType='" + columnType + '\'' +
                ", columnKey='" + columnKey + '\'' +
                ", nullable='" + nullable + '\'' +
                ", columnDefault='" + columnDefault + '\'' +
                ", ordinalPosition=" + ordinalPosition +
                '}';
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    public String getNullable() {
        return nullable;
    }

    public void setNullable(String nullable) {
        this.nullable = nullable;
    }

    public String getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(String columnDefault) {
        this.columnDefault = columnDefault;
    }

    public Long getOrdinalPosition() {
        return ordinalPosition;
    }

    public void setOrdinalPosition(Long ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
    }
}