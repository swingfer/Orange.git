package com.swing.sky.web.generator.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.swing.sky.web.generator.config.GenConfig;
import com.swing.sky.web.generator.config.MybatisConfig;
import com.swing.sky.web.generator.constant.ModuleConstants;
import com.swing.sky.web.generator.dao.GenDAO;
import com.swing.sky.web.generator.domain.Column;
import com.swing.sky.web.generator.domain.Module;
import com.swing.sky.web.generator.domain.ModuleHouse;

/**
 * @author swing
 */
public class ModuleHouseInitializer {

    public static List<ModuleHouse> buildModuleHouses() throws IOException {
        GenDAO genDAO = MybatisConfig.getDAO();
        List<ModuleHouse> moduleHouses = new ArrayList<>();
        // 数据库名
        String schemaName = Configs.readSky("schemaName");
        for (int i = 1; i < 100; i++) {
            String tableName = Configs.readSky("table_" + i);
            if (tableName == null) {
                continue;
            }
            ModuleHouse moduleHouse = new ModuleHouse();
            // 获取该moduleHouse中的所有module
            List<Module> modules = null;
            if ("on".equalsIgnoreCase(Configs.readSky("table_" + i + ".basic.enable"))) {
                modules = getModules(1);
            } else {
                modules = getModules(2);
            }

            String tableComment = genDAO.getTableComment(schemaName, tableName);
            List<Column> columns = genDAO.listColumns(schemaName, tableName);
            columns.sort((a, b) -> a.getColumnName().compareTo(b.getColumnName()));
            if (tableComment == null || columns == null) {
                System.out.println(tableName + "表不存在，请检查配置");
            }

            MapUtils.setFileNameByTableName(tableName, modules);
            moduleHouse.setTableSchema(schemaName);
            moduleHouse.setTableName(tableName);
            moduleHouse.setTableComment(tableComment);
            moduleHouse.setModules(modules);
            moduleHouse.setColumns(MapUtils.getGenColumnsByColumns(columns));
            moduleHouse.setPrimaryKey(MapUtils.getPrimaryKeyByColumns(columns));
            moduleHouse.setUniqueIndexs(MapUtils.getUniqueIndexsByColumns(columns));
            moduleHouse.setCommonColumns(MapUtils.getCommonByColumns(columns));
            moduleHouses.add(moduleHouse);
        }
        // relate表的处理

        for (int i = 1; i < 100; i++) {
            String tableName = Configs.readSky("relate_table_" + i);
            if (tableName == null) {
                continue;
            }
            List<Module> modules = getModules(3);
            MapUtils.setFileNameByTableName(tableName, modules);

            String tableComment = genDAO.getTableComment(schemaName, tableName);
            List<Column> columns = genDAO.listColumns(schemaName, tableName);
            columns.sort((a, b) -> a.getColumnName().compareTo(b.getColumnName()));
            if (tableComment == null || columns == null) {
                System.out.println(tableName + "表不存在，请检查配置");
            }

            ModuleHouse moduleHouse = new ModuleHouse();
            moduleHouse.setTableSchema(schemaName);
            moduleHouse.setTableName(tableName);
            moduleHouse.setTableComment(tableComment);
            moduleHouse.setModules(modules);
            moduleHouse.setColumns(MapUtils.getGenColumnsByColumns(columns));
            moduleHouse.setPrimaryKey(MapUtils.getPrimaryKeyByColumns(columns));
            moduleHouse.setUniqueIndexs(MapUtils.getUniqueIndexsByColumns2(columns));
            moduleHouse.setCommonColumns(MapUtils.getCommonByColumns(columns));
            moduleHouses.add(moduleHouse);
        }

        return moduleHouses;
    }

    /**
     * 当table.basic.enable=on时需要的模块
     */
    public static List<Module> getModules(int type) {
        List<String> moduleNames = new ArrayList<>();
        List<Module> modules = new ArrayList<>();
        if (type == 1) {
            moduleNames.add(ModuleConstants.BASIC_DOMAIN);
            moduleNames.add(ModuleConstants.DOMAIN_BASIC);
            moduleNames.add(ModuleConstants.BASIC_DAO);
            moduleNames.add(ModuleConstants.DAO_BASIC);
            moduleNames.add(ModuleConstants.BASIC_MAPPER);
            moduleNames.add(ModuleConstants.MAPPER_BASIC);
            moduleNames.add(ModuleConstants.BASIC_SERVICE);
            moduleNames.add(ModuleConstants.SERVICE_SUPER);
            moduleNames.add(ModuleConstants.SERVICE_IMPL);
        }
        if (type == 2) {
            moduleNames.add(ModuleConstants.DOMAIN);
            moduleNames.add(ModuleConstants.BASIC_DAO);
            moduleNames.add(ModuleConstants.DAO_BASIC);
            moduleNames.add(ModuleConstants.MAPPER);
            moduleNames.add(ModuleConstants.BASIC_SERVICE);
            moduleNames.add(ModuleConstants.SERVICE_SUPER);
            moduleNames.add(ModuleConstants.SERVICE);
        }
        if (type == 3) {
            moduleNames.add(ModuleConstants.DOMAIN);
            moduleNames.add(ModuleConstants.RELATE_DAO);
            moduleNames.add(ModuleConstants.RELATE_MAPPER);
            // moduleNames.add(ModuleConstants.BASIC_SERVICE);
            // moduleNames.add(ModuleConstants.SERVICE_SUPER);
            // moduleNames.add(ModuleConstants.SERVICE);
        }
        for (String moduleName : moduleNames) {
            modules.add(GenConfig.getModule(moduleName));
        }
        return modules;
    }

}
