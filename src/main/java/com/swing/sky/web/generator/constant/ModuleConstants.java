package com.swing.sky.web.generator.constant;

/**
 * 生成文件的模块表示
 *
 * @author swing
 */
public class ModuleConstants {
    public static final String BASIC_DOMAIN = "BasicDo";
    public static final String DOMAIN = "Do";
    public static final String DOMAIN_BASIC = "DoBasic";

    public static final String BASIC_DAO = "BasicDao";
    public static final String DAO = "Dao";
    public static final String DAO_BASIC = "DaoBasic";
    public static final String BASIC_LINK_DAO = "BasicLinkDao";
    public static final String DAO_LINK = "DaoLink";

    public static final String BASIC_SERVICE = "BasicService";
    public static final String SERVICE = "Service";
    public static final String SERVICE_SUPER = "ServiceSuper";
    public static final String SERVICE_IMPL = "ServiceImpl";
    
    public static final String RELATE_DAO = "RelateDao";
    public static final String RELATE_MAPPER = "RelateMapper";
    
    public static final String BASIC_MAPPER = "BasicMapper";
    public static final String MAPPER = "Mapper";
    public static final String MAPPER_BASIC = "MapperBasic";
    public static final String LINK_MAPPER = "LinkMapper";
}
