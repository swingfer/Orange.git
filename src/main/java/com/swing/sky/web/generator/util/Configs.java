package com.swing.sky.web.generator.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.swing.sky.web.generator.config.GenConfig;

/**
 * 读取配置文件工具
 *
 * @author swing
 */
public class Configs {
    public static Properties orangeProperties = null;
    public static Properties genProperties = null;

    /**
     * 读取properties文件内容
     */
    public static String readGen(String key) {
        if (genProperties == null) {
            // 建立Properties对象
            genProperties = new Properties();
            // 获取文件流
            InputStream inputStream = GenConfig.class.getClassLoader().getResourceAsStream("gen.properties");
            // 加载文件流
            try {
                genProperties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return genProperties.getProperty(key);
    }

    /**
     * 读取properties文件内容
     */
    public static String readSky(String key) {
        return getOrangeProperties().getProperty(key);
    }

    public static Properties getOrangeProperties() {
        if (orangeProperties == null) {
            // 建立Properties对象
            orangeProperties = new Properties();
            String myArgValue = System.getProperty("cfgPath", "src/main/resources/orange.properties");
            String configPath = System.getProperty("user.dir") + OsUtil.getSeparator() + myArgValue;
            // 获取文件流
            try (FileInputStream input = new FileInputStream(configPath)) {
                orangeProperties.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return orangeProperties;
    }
}
