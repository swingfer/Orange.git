package com.swing.sky.web.generator.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.swing.sky.web.generator.dao.GenDAO;
import com.swing.sky.web.generator.util.Configs;

/**
 * @author swing
 */
public class MybatisConfig {
    public static GenDAO getDAO() throws IOException {

        Properties orangeProperties = Configs.getOrangeProperties();

        // 读取mybatis-config.xml文件
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");

        // 初始化mybatis，创建SqlSessionFactory类的实例
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, orangeProperties);

        // 创建Session实例
        SqlSession session = sqlSessionFactory.openSession();

        // 获得mapper接口的代理对象
        return session.getMapper(GenDAO.class);
    }
}
