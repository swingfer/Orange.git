package com.swing.sky.web.generator.domain;

/**
 * @author swing
 */
public class GenColumn extends Column {
    /**
     * 字段对应的属性名
     */
    private String propertyName;

    /**
     * 属性在get/set方法名中的名字
     */
    private String getSetName;

    /**
     * 属性的java类型
     */
    private String propertyType;

    /**
     * mybatis中对应的jdbcType
     */
    private String jdbcType;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getGetSetName() {
        return getSetName;
    }

    public void setGetSetName(String getSetName) {
        this.getSetName = getSetName;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public GenColumn() {

    }

    public GenColumn(Column column) {
        setColumnName(column.getColumnName());
        setColumnComment(column.getColumnComment());
        setDataType(column.getDataType());
        setColumnType(column.getColumnType());
        setColumnKey(column.getColumnKey());
        setNullable(column.getNullable());
        setColumnDefault(column.getColumnDefault());
        setOrdinalPosition(column.getOrdinalPosition());
    }
}
