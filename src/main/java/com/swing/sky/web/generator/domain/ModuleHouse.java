package com.swing.sky.web.generator.domain;

import java.util.List;

/**
 * 揽括所有配置的核心(包括一个表和它所有的生成文件集合）
 *
 * @author swing
 */
public class ModuleHouse {
    /**
     * 数据库名
     */
    private String tableSchema;
    /**
     * 表名
     */
    private String tableName;

    /**
     * 表描述
     */
    private String tableComment;

    /**
     * 该表的列集合
     */
    private List<GenColumn> columns;

    /**
     * 主键
     */
    private GenColumn primaryKey;

    /**
     * 唯一索引
     */
    private List<GenColumn> uniqueIndexs;

    /**
     * 非唯一列（用来做条件检索）
     */
    private List<GenColumn> commonColumns;

    /**
     * 该表需要生成的文件模块集合
     */
    private List<Module> modules;

    private GenColumn leftColumn;
    private GenColumn rightColumn;

    /**
     * 左边的邻居名
     */
    private String leftHouseName;
    /**
     * 右边的邻居名
     */
    private String rightHouseName;

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public String getLeftHouseName() {
        return leftHouseName;
    }

    public void setLeftHouseName(String leftHouseName) {
        this.leftHouseName = leftHouseName;
    }

    public String getRightHouseName() {
        return rightHouseName;
    }

    
    public void setRightHouseName(String rightHouseName) {
        this.rightHouseName = rightHouseName;
    }

    public GenColumn getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(GenColumn primaryKey) {
        this.primaryKey = primaryKey;
    }

    public List<GenColumn> getUniqueIndexs() {
        return uniqueIndexs;
    }

    public void setUniqueIndexs(List<GenColumn> uniqueIndexs) {
        this.uniqueIndexs = uniqueIndexs;
    }

    public List<GenColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<GenColumn> columns) {
        this.columns = columns;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public List<GenColumn> getCommonColumns() {
        return commonColumns;
    }

    public void setCommonColumns(List<GenColumn> commonColumns) {
        this.commonColumns = commonColumns;
    }

    public GenColumn getLeftColumn() {
        return leftColumn;
    }

    public void setLeftColumn(GenColumn leftColumn) {
        this.leftColumn = leftColumn;
    }

    public GenColumn getRightColumn() {
        return rightColumn;
    }

    public void setRightColumn(GenColumn rightColumn) {
        this.rightColumn = rightColumn;
    }

}
